# Extensión para buscar términos en el portal DARA desde el menu contextual

# Descripción
Extensión para el navegador Chrome que permite buscar términos en el portal [DARA](http://dara.aragon.es/opac/app/home/) (Documentos y Archivos de Aragón) directamente desde el menu contextual del texto seleccionado en la página actual. Evita el tener que ir a la página del navegador, rellenar el campo de texto y pulsar el botón de buscar

# Uso
Simplemente seleccione un texto en la página actual del navegador, haga click con el botón derecho para mostrar el menu contextual y seleccione la opción **buscar en dara este texto**. Se abrirá el portal en la página de resultados de la búsqueda con el término seleccionado.
 